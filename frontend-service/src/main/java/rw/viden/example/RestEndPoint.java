package rw.viden.example;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;



@RestController
public class RestEndPoint {

	private final WebClient webClient;

	public RestEndPoint(WebClient webClient) {
		this.webClient = webClient;
	}

	@GetMapping("/message")
	public Mono<String> endpoint() {
		return webClient.get().uri("/backend/message").retrieve().bodyToMono(String.class);
	}
}
